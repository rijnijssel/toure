export function format(value) {
    return new Date(value).toLocaleDateString('nl-nl', {
        weekday: "long",
        year: "numeric",
        month: "short",
        day: "numeric"
    })
}

export default {
    format,
}

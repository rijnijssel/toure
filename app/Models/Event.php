<?php

namespace App\Models;

use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Date;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Event extends Model
{
    use HasFactory;
    use HasSlug;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'registration_start',
        'registration_end',
        'start',
    ];

    protected $casts = [
        'registration_start' => 'date',
        'registration_end' => 'date',
        'start' => 'date',
    ];

    public function beforeRegistrationPeriod(): bool
    {
        return $this->getRegistrationPeriod()->startsAfter();
    }


    public function afterRegistrationPeriod(): bool
    {
        return $this->getRegistrationPeriod()->endsBefore();
    }

    public function isRegistrationOpen(): bool
    {
        $period = $this->getRegistrationPeriod();
        return $period->isInProgress();
    }

    public function getRegistrationPeriod(): CarbonPeriod
    {
        return CarbonPeriod::create($this->registration_start->startOfDay(), $this->registration_end->endOfDay());
    }

    public function registrations(): HasMany
    {
        return $this->hasMany(Registration::class);
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}

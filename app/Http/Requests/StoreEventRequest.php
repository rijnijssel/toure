<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'max:255'],
            'description' => ['required', 'min:10'],
            'registration_start' => ['required', 'date'],
            'registration_end' => ['required', 'date'],
            'start' => ['required', 'date'],
        ];
    }
}

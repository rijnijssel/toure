<?php

namespace App\Http\Requests;

use App\Models\Event;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRegistrationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'event' => [
                'required',
                'int',
                Rule::exists(Event::class, 'id')
            ],
        ];
    }
}

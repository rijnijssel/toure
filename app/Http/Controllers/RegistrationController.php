<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRegistrationRequest;
use App\Models\Event;
use App\Models\Registration;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Inertia\Response;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        /* @var User $user */
        $user = Auth::user()
            ->loadMissing('events');

        return inertia('Registration/Index', [
            'events' => $user->events->transform(fn(Event $event) => [
                'id' => $event->id,
                'name' => $event->name,
                'slug' => $event->slug,
                'registration_start' => $event->registration_start,
                'registration_end' => $event->registration_end,
                'start' => $event->start,
                'is_registered' => $user->events->contains($event),
            ]),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRegistrationRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRegistrationRequest $request): RedirectResponse
    {
        $id = $request->validated('event');
        $event = Event::findOrFail($id);

        if (!$event->isRegistrationOpen()) {
            throw ValidationException::withMessages([
                'event' => trans('Inschrijvingen voor :event zijn gesloten', ['event' => $event->name]),
            ]);
        }

        $request->user()->registrations()->updateOrCreate([
            'event_id' => $event->id,
        ]);

        return redirect()->back()->with([
            'success' => trans('Je bent ingeschreven voor :event.', [
                'event' => $event->name,
            ]),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return RedirectResponse
     */
    public function destroy(Event $event): RedirectResponse
    {
        $user = Auth::user();
        $user->events()->detach($event);

        return redirect()->back()->with([
            'error' => trans('Je bent uitgeschreven voor :event.', [
                'event' => $event->name,
            ]),
        ]);
    }
}

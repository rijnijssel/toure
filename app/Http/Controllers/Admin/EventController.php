<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Models\Event;
use Illuminate\Http\RedirectResponse;
use Inertia\Response;

class EventController extends Controller
{
    public function index(): Response
    {
        $events = Event::query()
            ->withCount('registrations')
            ->orderBy('start', 'desc')
            ->get();

        return inertia('Admin/Event/Index', [
            'events' => $events->transform(fn (Event $event) => [
                'id' => $event->id,
                'name' => $event->name,
                'slug' => $event->slug,
                'description' => nl2br(str($event->description)->limit()),
                'registration_start' => $event->registration_start->toDateString(),
                'registration_end' => $event->registration_end->toDateString(),
                'registration_count' => $event->registrations_count,
                'start' => $event->start->toDateString(),
            ]),
        ]);
    }

    public function create(): Response
    {
        return inertia('Admin/Event/Form');
    }

    public function store(StoreEventRequest $request): RedirectResponse
    {
        $data = $request->validated();

        Event::query()->create([
            'name' => $data['name'],
            'description' => $data['description'],
            'registration_start' => $data['registration_start'],
            'registration_end' => $data['registration_end'],
            'start' => $data['start'],
        ]);

        return to_route('admin.events.index');
    }

    public function edit(Event $event): Response
    {
        return inertia('Admin/Event/Form', [
            'event' => [
                'name' => $event->name,
                'slug' => $event->slug,
                'description' => $event->description,
                'registration_start' => $event->registration_start->toDateString(),
                'registration_end' => $event->registration_end->toDateString(),
                'start' => $event->start->toDateString(),
            ],
        ]);
    }

    public function update(UpdateEventRequest $request, Event $event): RedirectResponse
    {
        $data = $request->validated();

        $event->update([
            'name' => $data['name'],
            'description' => $data['description'],
            'registration_start' => $data['registration_start'],
            'registration_end' => $data['registration_end'],
            'start' => $data['start'],
        ]);

        return to_route('admin.events.index');
    }

    public function destroy(Event $event): RedirectResponse
    {
        $event->registrations()->forceDelete();
        $event->forceDelete();

        return to_route('admin.events.index');
    }
}

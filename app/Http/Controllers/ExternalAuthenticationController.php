<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;

class ExternalAuthenticationController extends Controller
{
    public function create(): RedirectResponse
    {
        return Socialite::driver('gitlab')->redirect();
    }

    public function store(): RedirectResponse
    {
        try {
            /** @var \Laravel\Socialite\Two\User $info */
            $info = Socialite::driver('gitlab')->user();

            $user = User::query()->updateOrCreate([
                'gitlab_user_id' => $info->getId(),
            ], [
                'is_admin' => data_get($info, 'is_admin', false),
            ]);

            Auth::login($user);

            Session::put('user', [
                'name' => $info->getName(),
                'email' => $info->getEmail(),
                'profile_url' => data_get($info, 'web_url'),
                'avatar_url' => $info->getAvatar() ?? asset('img/no-avatar.svg'),
            ]);
        } catch (InvalidStateException $exception) {
            Log::error($exception);
        }

        return redirect('/');
    }
}

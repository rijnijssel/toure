<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateEventRequest;
use App\Models\Event;
use Illuminate\Support\Facades\Auth;
use Inertia\Response;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        $user = Auth::user()->loadMissing('registrations');

        $events = Event::query()
            ->orderBy('start')
            ->get();

        return inertia('Event/Index', [
            'events' => $events->transform(fn(Event $event) => [
                'id' => $event->id,
                'name' => $event->name,
                'slug' => $event->slug,
                'registration_start' => $event->registration_start,
                'registration_end' => $event->registration_end,
                'start' => $event->start,
                'is_registered' => $user->events->contains($event),
            ]),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Event $event
     * @return Response
     */
    public function show(Event $event): Response
    {
        $user = Auth::user()->loadMissing('events');
        $event->loadCount('registrations');

        return inertia('Event/Show', [
            'event' => [
                'id' => $event->id,
                'name' => $event->name,
                'slug' => $event->slug,
                'description' => nl2br($event->description),
                'start' => $event->start,
                'registration' => [
                    'count' => $event->registrations_count,
                    'start' => $event->registration_start,
                    'end' => $event->registration_end,
                    'opened' => $event->isRegistrationOpen(),
                    'before' => $event->beforeRegistrationPeriod(),
                    'after' => $event->afterRegistrationPeriod(),
                ],
                'is_registered' => $user->events->contains($event),
            ],
        ]);
    }
}

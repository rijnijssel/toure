<?php

use App\Http\Controllers\EventController;
use App\Http\Controllers\ExternalAuthenticationController;
use App\Http\Controllers\RegistrationController;
use App\Models\Registration;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::inertia('/', 'Welcome')->name('welcome');

Route::middleware('guest')->group(function () {
    Route::get('/sso/redirect', [ExternalAuthenticationController::class, 'create'])->name('sso.redirect');
    Route::get('/sso/callback', [ExternalAuthenticationController::class, 'store']);
});

Route::middleware('auth')->group(function () {
    Route::resource('events', App\Http\Controllers\EventController::class)->only(['index', 'show']);

    Route::get('/events', [EventController::class, 'index'])->name('events.index');
    Route::get('/events/{event}', [EventController::class, 'show'])->name('events.show');

    Route::get('/registrations', [RegistrationController::class, 'index'])->name('registrations.index');
    Route::post('/registrations', [RegistrationController::class, 'store'])->name('registrations.store');
    Route::delete('/registrations/{event}', [RegistrationController::class, 'destroy'])->name('registrations.destroy');

    Route::prefix('admin')->group(function () {
        Route::get('/', function () {
            return to_route('admin.events.index');
        });

        Route::resource('events', App\Http\Controllers\Admin\EventController::class)
            ->names('admin.events');
    });
});

Route::get('/debug', function (Store $session) {
    return response([
        'user' => auth()->check() ? [
            'name' => $session->get('user.name'),
            'email' => $session->get('user.email'),
            'profile_url' => $session->get('user.profile_url'),
            'avatar_url' => $session->get('user.avatar_url'),
        ] : null,
        'registrations' => auth()->user()?->registrations
                ->load([
                    'event' => fn (BelongsTo $builder) => $builder->withCount('registrations'),
                ])
                ->map(fn (Registration $registration) => [
                    'event' => [
                        'name' => $registration->event->name,
                        'slug' => $registration->event->slug,
                        'registrations' => $registration->event->registrations_count,
                        'links' => [
                            'show' => route('events.show', $registration->event),
                        ],
                    ],
                ]) ?? null,
    ]);
});

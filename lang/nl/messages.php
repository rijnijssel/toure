<?php

return [
    'registration' => [
        'menu' => 'Inschrijvingen',
        'opens_at' => 'Inschrijving opent op :start',
    ],
];
